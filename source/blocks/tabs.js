(function () {
    function tabs(selector){
        // console.log( Array.from(document.querySelectorAll(selector)));
        Array.from(document.querySelectorAll(selector)).forEach(function(tabs){
            console.log(tabs);
            let tabContent = tabs.parentElement.querySelector('.tab-content');

            let tabsLink = tabs.children;
            let tabContents = tabContent.children;

            if(!tabs) return;
            tabs.addEventListener('click', function (e){
                onTabClick(e.target);
            });

            tabInit();

            function tabInit(){
               let currentTab = tabs.querySelector('.active');
               if(!currentTab)
                   currentTab = tabsLink[0];
                onTabClick(currentTab);
            }

            function onTabClick(tabEl) {
                let id = tabEl.dataset.id;
                if(!id) return; //if id dont find dont do anything

                //unactive and hide all tabs
                [...tabsLink].forEach(link=>{
                    if(link.classList.contains('active'))
                    link.classList.remove('active');
                });
                [...tabContents].forEach(content=>content.style.display = 'none');

                //show and active current tab
                document.getElementById(id).style.display = 'block';
                tabEl.classList.add('active');
            }
        })
    }
    window.tabs = tabs;
})();