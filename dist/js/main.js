document.addEventListener('DOMContentLoaded', ready)
function ready() {
    let works = {
        graphic: [
            {
                img: 'images/graphic/graphic-design1.jpg',
                title: 'Graphic Design',
                subtitle: 'Graphic Design'
            },
            {
                img: 'images/graphic/graphic-design2.jpg',
                title: 'Graphic Design',
                subtitle: 'Graphic Design'
            },
            {
                img: 'images/graphic/graphic-design3.jpg',
                title: 'Graphic Design',
                subtitle: 'Graphic Design'
            },
            {
                img: 'images/graphic/graphic-design4.jpg',
                title: 'Graphic Design',
                subtitle: 'Graphic Design'
            },
            {
                img: 'images/graphic/graphic-design5.jpg',
                title: 'Graphic Design',
                subtitle: 'Graphic Design'
            },
            {
                img: 'images/graphic/graphic-design6.jpg',
                title: 'Graphic Design',
                subtitle: 'Graphic Design'
            },
            {
                img: 'images/graphic/graphic-design7.jpg',
                title: 'Graphic Design',
                subtitle: 'Graphic Design'
            },
            {
                img: 'images/graphic/graphic-design8.jpg',
                title: 'Graphic Design',
                subtitle: 'Graphic Design'
            },
            {
                img: 'images/graphic/graphic-design9.jpg',
                title: 'Graphic Design',
                subtitle: 'Graphic Design'
            },
            {
                img: 'images/graphic/graphic-design10.jpg',
                title: 'Graphic Design',
                subtitle: 'Graphic Design'
            },
            {
                img: 'images/graphic/graphic-design11.jpg',
                title: 'Graphic Design',
                subtitle: 'Graphic Design'
            },
            {
                img: 'images/graphic/graphic-design12.jpg',
                title: 'Graphic Design',
                subtitle: 'Graphic Design'
            },
        ],
        web: [
            {
                img: 'images/web/web-design1.jpg',
                title: 'Web Design',
                subtitle: 'Web Design'
            },
            {
                img: 'images/web/web-design2.jpg',
                title: 'Web Design',
                subtitle: 'Web Design'
            },
            {
                img: 'images/web/web-design3.jpg',
                title: 'Web Design',
                subtitle: 'Web Design'
            },
            {
                img: 'images/web/web-design4.jpg',
                title: 'Web Design',
                subtitle: 'Web Design'
            },
            {
                img: 'images/web/web-design5.jpg',
                title: 'Web Design',
                subtitle: 'Web Design'
            },
            {
                img: 'images/web/web-design6.jpg',
                title: 'Web Design',
                subtitle: 'Web Design'
            },
            {
                img: 'images/web/web-design7.jpg',
                title: 'Web Design',
                subtitle: 'Web Design'
            },

        ],
        landing: [
            {
                img: 'images/landing/landing-page1.jpg',
                title: 'Landing Pages',
                subtitle: 'Landing Pages'
            },
            {
                img: 'images/landing/landing-page2.jpg',
                title: 'Landing Pages',
                subtitle: 'Landing Pages'
            },
            {
                img: 'images/landing/landing-page3.jpg',
                title: 'Landing Pages',
                subtitle: 'Landing Pages'
            },
            {
                img: 'images/landing/landing-page4.jpg',
                title: 'Landing Pages',
                subtitle: 'Landing Pages'
            },
            {
                img: 'images/landing/landing-page5.jpg',
                title: 'Landing Pages',
                subtitle: 'Landing Pages'
            },
            {
                img: 'images/landing/landing-page6.jpg',
                title: 'Landing Pages',
                subtitle: 'Landing Pages'
            },
            {
                img: 'images/landing/landing-page7.jpg',
                title: 'Landing Pages',
                subtitle: 'Landing Pages'
            },

        ],
        wp: [
            {
                img: 'images/wordpress/wordpress1.jpg',
                title: 'Wordpress',
                subtitle: 'Wordpress'
            },
            {
                img: 'images/wordpress/wordpress2.jpg',
                title: 'Wordpress',
                subtitle: 'Wordpress'
            },
            {
                img: 'images/wordpress/wordpress3.jpg',
                title: 'Wordpress',
                subtitle: 'Wordpress'
            },
            {
                img: 'images/wordpress/wordpress4.jpg',
                title: 'Wordpress',
                subtitle: 'Wordpress'
            },
            {
                img: 'images/wordpress/wordpress5.jpg',
                title: 'Wordpress',
                subtitle: 'Wordpress'
            },
            {
                img: 'images/wordpress/wordpress6.jpg',
                title: 'Wordpress',
                subtitle: 'Wordpress'
            },
            {
                img: 'images/wordpress/wordpress7.jpg',
                title: 'Wordpress',
                subtitle: 'Wordpress'
            },
            {
                img: 'images/wordpress/wordpress8.jpg',
                title: 'Wordpress',
                subtitle: 'Wordpress'
            },
            {
                img: 'images/wordpress/wordpress9.jpg',
                title: 'Wordpress',
                subtitle: 'Wordpress'
            },
            {
                img: 'images/wordpress/wordpress10.jpg',
                title: 'Wordpress',
                subtitle: 'Wordpress'
            },

        ]
    };
    tabs('.tabs', works);
    slider('.slider')
}