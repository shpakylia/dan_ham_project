(function () {
    function sort(items, el, count = 12){
        let sortType = el.dataset.sort;
        let itemsData = [];
        const loader = createLoader();

        if(sortType === 'all'){
            itemsData = Object.values(items).reduce((acc, item) => [...acc, ...item], []);
        }
        else{
            if(!items[sortType]) return;
            itemsData = items[sortType];
        }
        let container = document.querySelector(`.tab-pane[data-sort="${sortType}"]`);
        if(!container || itemsData.length < 1) return;
        container.innerHTML = '';

        displayItems();

        [...document.querySelectorAll('.load-more-btn')].forEach((moreBtn)=>{
            console.log(moreBtn);
            moreBtn.addEventListener('click', function(e){
                let btn = null;
                console.log(e);
                if(e){
                    btn = e.currentTarget;
                    btn.remove();
                }
                container.append(loader);
                setTimeout(function () {
                    loader.remove();
                    displayItems(btn);
                }, 2000, btn)
            })
        });


        function createItem(itemData) {
            return `
            <div class="works-item">
                <img src="${itemData.img}">
                <div class="works-item__desc">
                    <div class="works-item__links links">
                        <a href="#" class="link">
                            <i class="fa fa-link"></i>
                        </a>
                        <a href="#" class="link">
                            <i class="fa fa-search"></i>
                        </a>
                    </div>
                    <p class="works-item__title">${itemData.title}</p>
                    <p class="works-item__sutitle">${itemData.subtitle}</p>
                </div>
            </div>
            `;
        }
        function getBtnMore(){
            return `
            <div class="load-btn">
                <span class="btn btn-success load-more-btn">
                    <i class="fa fa-plus"></i>
                    Load More
                </span>
            </div>
            `;
        }
        function displayItems(btn){
            console.log(btn);
            let childs = container.children;
            if(childs.length < itemsData.length){
                let counting = 0;
                for (let i = childs.length; i < itemsData.length; i++){
                    if(counting >= count){
                        if(btn){
                            container.append(btn);
                        }else{
                            container.innerHTML += getBtnMore();
                        }
                        break;
                    }
                    container.innerHTML += createItem(itemsData[i]);
                    counting++;
                }

            }

        }
        function createLoader() {
            let loader = document.createElement('div');
            loader.className = "loading";
            loader.innerHTML =
             `
                <div></div>
                <div></div>
                <div></div>
            `;
            return loader;

        }

    }
    window.sort = sort;
})();