(function () {
    function slider(selector, slidesToShow = 4, itemWeight = "98px"){
        Array.from(document.querySelectorAll(selector)).forEach(function(slider){
            const btnPrev = slider.querySelector('.btn-prev');
            const btnNext = slider.querySelector('.btn-next');
            const slideBlock = slider.querySelector(".slider-items");
            const slides = slideBlock.children;
            let currentSlide = 0;

            btnPrev.addEventListener('click', previousSlide);
            btnNext.addEventListener('click', nextSlide);
            slideBlock.addEventListener('click', function (e) {
                let clickedItem = e.target;
                currentSlide = [...slides].indexOf(clickedItem);
            });

            function nextSlide() {

                if(currentSlide+1 > slides.length-1) return;
                goToSlide(currentSlide+1);

            }

            function previousSlide() {

                if(currentSlide-1 < 0) return;
                goToSlide(currentSlide-1);
            }

            function goToSlide(n) {
                if(slides[currentSlide].classList.contains('active'))
                    slides[currentSlide].classList.remove('active');
                currentSlide = (n+slides.length)%slides.length;
                slides[currentSlide].classList.add('active');
                if(currentSlide > slidesToShow-1){
                    slideBlock.style.left = `${-98*(currentSlide - (slidesToShow-1))}px`;
                }
                else{
                    slideBlock.style.left = '0';
                }
                slides[currentSlide].click();

            }

        })
    }
    window.slider = slider;
})();