(function () {
    function tabs(selector, sortArr = {}){
        // console.log( Array.from(document.querySelectorAll(selector)));
        Array.from(document.querySelectorAll(selector)).forEach(function(tabs){
            let tabContent = tabs.closest('.tab-container').querySelector('.tab-content');

            let tabsLink = tabs.querySelectorAll('.tabs-title');
            let tabContents = tabContent.querySelectorAll(".tab-pane");

            if(!tabs) return;
            tabs.addEventListener('click', function (e){
                onTabClick(e.target);
                if(Object.keys(sortArr).length > 0) {
                    sort(sortArr, e.target);
                }

            });

            tabInit();

            function tabInit(){
               let currentTab = tabs.querySelector('.active');
               if(!currentTab)
                   currentTab = tabsLink[0];
                currentTab.click();
            }

            function onTabClick(tabEl) {
                let id = tabEl.dataset.id;
                if(!id) return; //if id dont find dont do anything

                //unactive and hide all tabs
                [...tabsLink].forEach(link=>{
                    if(link.classList.contains('active'))
                    link.classList.remove('active');
                });
                [...tabContents].forEach(content=>{
                    content.style.display = 'none';
                    if(content.classList.contains('active'))
                        content.classList.remove('active');

                });

                //show and active current tab
                let curContainer = document.getElementById(id);
                curContainer.style.display = 'flex';
                setTimeout(function (){
                    curContainer.classList.add('active');
                }, 500);
                // curContainer.classList.add('active');
                tabEl.classList.add('active');

            }
        })
    }
    window.tabs = tabs;
})();